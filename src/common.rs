use crate::game::card::parser::action::Action;
use bigdecimal::BigDecimal;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use tyenum::tyenum;

pub type KeyValuePairs = HashMap<String, Value>;
pub type EventHandlers = HashMap<String, Action>;

#[tyenum]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Value {
    Empty(()),
    String,
    BigDecimal,
}
