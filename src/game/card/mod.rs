pub mod parser;
use crate::common::{EventHandlers, KeyValuePairs};
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

#[derive(Clone, Deserialize, Serialize)]
pub struct CardSerDe {
    pub name: String,
    pub ty: Type,
    pub tags: HashSet<String>,
    pub description: String,
    pub flavour: Option<String>,
}

impl From<CardSerDe> for Card {
    fn from(card: CardSerDe) -> Self {
        let CardSerDe {
            name,
            ty,
            tags,
            description,
            flavour,
        } = card;

        Card {
            name,
            ty,
            description,
            flavour,
            key_value_paris: KeyValuePairs::new(),
            event_handlers: EventHandlers::new(),
        }
    }
}

#[derive(Debug)]
pub struct Card {
    pub name: String,
    pub ty: Type,
    pub description: String,
    pub flavour: Option<String>,
    pub key_value_paris: KeyValuePairs,
    pub event_handlers: EventHandlers,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Type {
    Action,
    Thing,
}
