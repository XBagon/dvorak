use super::Parse;
use crate::common::Value;
use crate::game::card::parser::{Error, Words};
use crate::game::Game;
use num::rational::Ratio;
use tyenum::tyenum;

pub trait Execute {
    fn execute(&self, game: &mut Game) -> Value;
}

#[tyenum(derive=[Execute])]
#[derive(Debug)]
pub enum Action {
    Sequence,
    If,
    Draw,
}

#[derive(Constructor, Debug)]
pub struct Sequence(Vec<Action>);

impl Execute for Sequence {
    fn execute(&self, game: &mut Game) -> Value {
        unimplemented!()
    }
}

impl Parse for Sequence {
    fn parse(mut words: &mut Words) -> Result<Self, Error> {
        let mut actions = Vec::new();
        let mut current;
        while let Ok(word) = words.next() {
            current = match word {
                "draw" => actions.push(Action::Draw(words.parse()?)),
                _ => return Err(Error::Unexpected(word.into())),
            }
        }

        Ok(Sequence::new(actions))
    }
}

#[derive(Debug)]
pub struct If {}

impl Execute for If {
    fn execute(&self, game: &mut Game) -> Value {
        unimplemented!()
    }
}

#[derive(Debug)]
pub struct Draw {
    amount: Amount,
}

impl Execute for Draw {
    fn execute(&self, game: &mut Game) -> Value {
        unimplemented!()
    }
}

impl Parse for Draw {
    fn parse(mut words: &mut Words) -> Result<Self, Error> {
        let amount = words.parse()?;
        words.consume_optional("cards");

        Ok(Draw {
            amount,
        })
    }
}

#[derive(Debug)]
enum Amount {
    Ratio(Ratio<u32>),
    Absolute(u32),
}

impl Parse for Amount {
    fn parse(words: &mut Words) -> Result<Self, Error> {
        let word = words.next()?;
        match word {
            "all" => Ok(Amount::Ratio(Ratio::new(1, 1))),
            "a" => Ok(Amount::Absolute(1)),
            word => {
                if let Ok(number) = word.parse() {
                    Ok(Amount::Absolute(number))
                } else if let Ok(ratio) = word.parse() {
                    Ok(Amount::Ratio(ratio))
                } else {
                    Err(Error::Unexpected(word.into()))
                }
            }
        }
    }
}
