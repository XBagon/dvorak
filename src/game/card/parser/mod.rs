pub mod action;

use action::*;
use std::io::BufRead;

struct Parser {}

impl Parser {
    fn parse(&mut self, input: &str) -> Result<Action, Error> {
        let mut words = Words::new(input);
        Ok(Action::Sequence(words.parse()?))
    }
}

struct Words {
    words: Vec<String>,
    index: usize,
}

impl Words {
    fn new(input: &str) -> Self {
        let mut words = Vec::new();
        let mut words_iter = input.split_whitespace();
        for word in words_iter {
            let (rest, last_char) = word.split_at(word.len() - 1);
            if last_char == "." || last_char == "," {
                words.push(rest.to_ascii_lowercase());
                words.push(last_char.into());
            } else {
                words.push(word.to_ascii_lowercase());
            }
        }

        Words {
            words,
            index: 0,
        }
    }

    fn next(&mut self) -> Result<&str, Error> {
        let ret = self.words.get(self.index).ok_or(Error::Empty).map(|v| &v[..]);
        self.index += 1;
        ret
    }

    fn peek(&self) -> Result<&str, Error> {
        self.words.get(self.index).ok_or(Error::Empty).map(|v| &v[..])
    }

    fn consume(&mut self, word: &str) -> Result<&str, Error> {
        let next = self.next()?;
        if next == word {
            Ok(next)
        } else {
            Err(Error::Consume(word.into()))
        }
    }

    fn consume_optional(&mut self, word: &str) -> Result<Option<&str>, Error> {
        if let Ok(next) = self.peek() {
            if next == word {
                return Ok(Some(next));
            } else {
                return Ok(None);
            }
        }
        Err(Error::Consume(word.into()))
    }

    fn consume_one(&mut self, words: &[&str]) -> Result<&str, Error> {
        let next = self.next().map_err(|_| Error::Consume(words.iter().map(|&s| String::from(s)).collect()))?;
        if words.contains(&next) {
            Ok(next)
        } else {
            Err(Error::Unexpected(next.into()))
        }
    }

    fn consume_one_optional(&mut self, words: &[&str]) -> Result<Option<&str>, Error> {
        let next = self.next().map_err(|_| Error::Consume(words.iter().map(|&s| String::from(s)).collect()))?;
        if words.contains(&next) {
            Ok(Some(next))
        } else {
            Ok(None)
        }
    }

    fn parse<T: Parse>(&mut self) -> Result<T, Error> {
        T::parse(self)
    }

    fn parse_optional<T: Parse>(&mut self) -> Option<T> {
        let index = self.index;
        if let Ok(parsed) = T::parse(self) {
            Some(parsed)
        } else {
            self.index = index;
            None
        }
    }
}

trait Parse
where
    Self: Sized,
{
    fn parse(words: &mut Words) -> Result<Self, Error>;
}

#[derive(Debug)]
enum Error {
    Empty,
    Consume(String),
    Unexpected(String),
}

struct Event {
    name: String,
}

impl Parse for Event {
    fn parse(words: &mut Words) -> Result<Self, Error> {
        unimplemented!()
    }
}
