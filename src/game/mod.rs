pub mod card;
pub mod player;
use crate::common::{EventHandlers, KeyValuePairs};
use card::Card;
use id_arena::Arena;
use player::Player;

pub struct Game {
    players: Arena<Player>,
    cards: Arena<Card>,
    key_value_pairs: KeyValuePairs,
    event_handlers: EventHandlers,
}

impl Game {
    pub fn new(player_count: usize, cards: Arena<Card>) -> Self {
        let mut players = Arena::new();
        players.alloc(Player::default());
        players.alloc(Player::default());
        Game {
            players,
            cards,
            key_value_pairs: KeyValuePairs::default(),
            event_handlers: EventHandlers::default(),
        }
    }
}
