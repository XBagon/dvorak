use super::card::Card;
use crate::common::{EventHandlers, KeyValuePairs};
use id_arena::Id;

#[derive(Default)]
pub struct Player {
    cards: Vec<Id<Card>>,
    key_value_pairs: KeyValuePairs,
    event_handlers: EventHandlers,
}

impl Player {}
