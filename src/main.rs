#[macro_use]
extern crate derive_more;

use crate::game::Game;
use id_arena::Arena;

mod common;
mod game;

fn main() {
    //create_card_helper();
    read_card_helper();
    let mut cards = Arena::new();
    let mut game = Game::new(2, cards);
}

fn create_card_helper() {
    use game::card::Type;
    use std::collections::HashSet;

    let card = game::card::CardSerDe {
        name: String::from("Drawing Pen"),
        ty: Type::Thing,
        tags: HashSet::default(),
        description: String::from(""),
        flavour: None,
    };
    let serialized = ron::ser::to_string_pretty(&card, ron::ser::PrettyConfig::default()).unwrap();
    std::fs::write("example_cards/drawing_pen.ron", serialized).unwrap();
}

fn read_card_helper() {
    use game::card::{Card, CardSerDe};
    let card_ron = std::fs::read_to_string("example_cards/drawing_pen.ron").unwrap();
    let card: CardSerDe = ron::de::from_str(&card_ron).unwrap();
    let card: Card = card.into();
    dbg!(card);
}
